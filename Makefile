# make project
CC = g++

LDLIBS  = -lGL   # Core OpenGL, functions with prefix gl
LDLIBS += -lglfw # window manager, input etc, replacement of (free) GLUT
LDLIBS += -lGLEW # OpenGL Extension Wrangler Library, c/cpp extension loading at run time

INCLUDES = -Isource 
CFLAGS = -g -lstdc++ -std=c++1z -Wno-write-strings #-DIMGUI_IMPL_OPENGL_LOADER_GLEW

SRC_DIR = source
BUILD_DIR = build

TARGET = project
TXT = genTextures

# Rules
all: $(TARGET)

$(TARGET).o: $(TARGET).cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@ $(LDLIBS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@ $(LDLIBS)

texture: $(TXT)
	./$(TXT)

$(TXT).o: $(TXT).cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@ $(LDLIBS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@ $(LDLIBS)


run: $(TARGET)
	MESA_GL_VERSION_OVERRIDE=4.5 ./$(TARGET)

clean:
	rm -f $(BUILD_DIR)/*.o *.o ./$(TARGET) imgui.ini

.PHONY: all clean

# dependencies
$(TARGET): $(BUILD_DIR)/imgui.o $(BUILD_DIR)/imgui_impl_glfw.o $(BUILD_DIR)/imgui_impl_opengl3.o $(BUILD_DIR)/imgui_demo.o $(BUILD_DIR)/imgui_draw.o $(BUILD_DIR)/imgui_widgets.o
