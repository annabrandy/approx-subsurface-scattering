# Readme

## Build instructions

Build and run with `make run`.

Needs at least the following libraries (tested on Debian 9)

* GLEW: `libglew-dev` and `libglew2.0`
* GLFW: `libglfw3` and `libglfw3-dev`
* OpenGL: Version 4.5

## Acknowledgements

The project uses the external, unmodified libraries [imGui](https://github.com/ocornut/imgui) and [stb_image](https://github.com/nothings/stb).

The models used in the project are mostly created with [Blender](https://www.blender.org), despite from the [Stanford Bunny](https://graphics.stanford.edu/data/3Dscanrep/), which I modified in Blender.