#include <iostream> // std in out
#include <fstream>  // file in out
using namespace std;

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp> // e.g. for perspective matrix
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
using namespace glm;

/* standard includes */
#include <cstdlib>
#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>

/** color constants */
const vec3 white = vec3(1, 1, 1);
const vec3 black = vec3(0);
const vec3 red = vec3(1, 0, 0);
const vec3 green = vec3(0, 1, 0);
const vec3 blue = vec3(0, 0, 1);

/** Converts from HSL to RGB color space. Therefore hue H in [0, 360],
    saturation S in [0, 1] and lightness L in [0, 1]. Source: Wiki 
    For vec3 and returns range [0, 1] */
vec3 hslToRgb(vec3 hsl)
{
    float H = hsl.x, S = hsl.y, L = hsl.z;

    float C = (1 - abs(2 * L - 1)) * S;
    float X = C * (1 - abs(fmod(H / 60.0, 2.0) - 1));
    float m = L - C / 2.0;

    vec3 rgb{0, 0, 0};
    if (H < 60)
        rgb = {C, X, 0};
    else if (H < 120)
        rgb = {X, C, 0};
    else if (H < 180)
        rgb = {0, C, X};
    else if (H < 240)
        rgb = {0, X, C};
    else if (H < 300)
        rgb = {X, 0, C};
    else
        rgb = {C, 0, X};

    for (int i = 0; i < 3; i++)
    {
        rgb[i] = (rgb[i] + m);
    }

    return rgb;
}

// Souce: https://lodev.org/cgtutor/randomnoise.html

class Texture
{
    private:
    int textureSize{128};
    vector<vector<vector<float>>> noise;
    vector<vec3> colors;
    const char *fileName;

    // type of noise
    int noiseType{5};
    bool useSmooth{true};
    bool useTurbulence{true};
    float turbSize{8};

    // noise settings for marble
    float xPeriod{1};
    float yPeriod{1};
    float turbPower{0};

    // settings for HSL
    int H = 0;
    float S = 1;
    float L = 1;
    float from = 0;
    float to = 1;

    /** Fills noise with random values between 0 and 1 */
    void generateNoise()
    {
        for (int i = 0; i < textureSize; i++)
        {
            vector<vector<float>> data1;
            for (int j = 0; j < textureSize; j++)
            {
                vector<float> data2;
                for (int k = 0; k < textureSize; k++)
                {
                    data2.push_back((rand() % 32768) / 32768.0);
                }
                data1.push_back(data2);
            }
            noise.push_back(data1);
        }
    }

    /** Returns noise value, but linearly interpolated between neighbors. Same domain */
    float smoothNoise(float x, float y, float z, float noiseSize)
    {
        x /= noiseSize;
        y /= noiseSize;
        z /= noiseSize;

        if (!useSmooth)
            return noise[int(x)][int(y)][int(z)];

        // get fractional part of x and y
        float fractX = x - int(x);
        float fractY = y - int(y);
        float fractZ = z - int(z);

        // wrap around
        int x1 = (int(x) + textureSize) % textureSize;
        int y1 = (int(y) + textureSize) % textureSize;
        int z1 = (int(z) + textureSize) % textureSize;

        // neighbor values
        int x2 = (x1 + textureSize - 1) % textureSize;
        int y2 = (y1 + textureSize - 1) % textureSize;
        int z2 = (z1 + textureSize - 1) % textureSize;

        // smooth the noise with bilinear interpolation
        float value = 0.0;
        value += fractX * fractY * fractZ * noise[x1][y1][z1];
        value += fractX * fractY * (1 - fractZ) * noise[x1][y1][z2];
        value += fractX * (1 - fractY) * fractZ * noise[x1][y2][z1];
        value += fractX * (1 - fractY) * (1 - fractZ) * noise[x1][y2][z2];

        value += (1 - fractX) * fractY * fractZ * noise[x2][y1][z1];
        value += (1 - fractX) * fractY * (1 - fractZ) * noise[x2][y1][z2];
        value += (1 - fractX) * (1 - fractY) * fractZ * noise[x2][y2][z1];
        value += (1 - fractX) * (1 - fractY) * (1 - fractZ) * noise[x2][y2][z2];

        return value;
    }

    /** Adds up smoothed noise to produce turbulent noise, domain [0, 1] */
    float turbulence(float x, float y, float z)
    {
        float value = 0;

        if (useTurbulence)
        {
            for (int i = 1; i <= turbSize; i *= 2.0)
            {
                value += smoothNoise(x, y, z, i) * i;
            }
            return value / (2 * turbSize);
        }
        else
        {
            return smoothNoise(x, y, z, turbSize);
        }

    }

    /** Use turbulence to vary lightness.
        - cloud: H = 192 (blue), S = 1, start = 0.75, range = 0.25
     */
    vec3 hslLTurbulence(float x, float y, float z)
    {
        L = mix(from, to, smoothstep(0.0f, 1.0f, turbulence(x, y, z)));
        return hslToRgb(vec3(H % 360, S, L));
    }

    /** Uses turbulence to vary hue. Extremes not really visible. */
    vec3 hslHTurbulence(float x, float y, float z)
    {
        H = mix(from, to, smoothstep(0.0f, 1.0f, turbulence(x, y, z)));
        return hslToRgb(vec3(H % 360, S, L));
    }

    vec3 hslSTurbulence(float x, float y, float z)
    {
        S = mix(from, to, smoothstep(0.0f, 1.0f, turbulence(x, y, z)));
        return hslToRgb(vec3(H % 360, S, L));
    }

    /** Marble noise. Uses turbulence and sine value, interpolate between all colors. Domain [0, 1]*/
    vec3 marbleNoise(float x, float y, float z)
    {
        float xyValue =
            x * xPeriod / textureSize +
            y * yPeriod / textureSize +
            turbPower * turbulence(x, y, z);

        float sineValue = fabs(sin(xyValue * M_PI));
        vec3 c = vec3(0);

        float frac = 1 / (float)(colors.size() - 1);
        for (int i = 1; i < colors.size(); i++)
        {
            if (sineValue < i * frac)
            {
                float smoothSineValue = smoothstep((float)(i - 1) * frac, (float)i * frac, sineValue);
                c = mix(colors[i - 1], colors[i], smoothSineValue);
                break;
            }
        }

        return c;
    }

    void generateTexture()
    {
        cout << "generate texture " << fileName << " ..." << endl;

        /* prepare file to write texture to */
        ofstream file;
        file.open("data/" + string(fileName));

        /* generate noise */
        generateNoise();

        /* fill file */
        for (int i = 0; i < textureSize; i++)
        {
            for (int j = 0; j < textureSize; j++)
            {
                for (int k = 0; k < textureSize; k++)
                {
                    vec3 color{0, 0, 0};

                    switch (noiseType)
                    {
                    case 0:
                        color = marbleNoise(i, j, k);
                        break;

                    case 1:
                        color = hslLTurbulence(i, j, k);
                        break;

                    case 2:
                        color = hslHTurbulence(i, j, k);
                        break;

                    case 3:
                        color = hslSTurbulence(i, j, k);
                        break;
                    }

                    color *= 255.0f;

                    file << int(color.x) << " ";
                    file << int(color.y) << " ";
                    file << int(color.z) << " ";
                }
                file << endl;
            }
        }

        file.close();
    }


    public:

    Texture(bool useSmooth_, bool useTurbulence_, 
            float turbSize_, int textureSize_ = 128) :
            useSmooth(useSmooth_), useTurbulence(useTurbulence_), 
            turbSize(turbSize_), textureSize(textureSize_) {}

    void generateMarble(const char *fileName_, float xPeriod_, 
        float yPeriod_, float turbPower_, vector<vec3> colors_)
    {
        fileName = fileName_;
        xPeriod = xPeriod_;
        yPeriod = yPeriod_;
        turbPower = turbPower_;
        colors = colors_;

        noiseType = 0;
        generateTexture();
    }

    void generateLTurbulence(const char *fileName_, int H_, float S_, float from_ = 0, float to_ = 1)
    {
        fileName = fileName_;
        H = H_;
        S = S_;
        from = from_;
        to = to_;

        noiseType = 1;
        generateTexture();
    }

    void generateHTurbulence(const char *fileName_, float S_, float L_, float from_ = 0, float to_ = 360)
    {
        fileName = fileName_;
        S = S_;
        L = L_;
        from = from_;
        to = to_;

        noiseType = 2;
        generateTexture();
    }

    void generateSTurbulence(const char *fileName_, int H_, float L_, float from_ = 0, float to_ = 1)
    {
        fileName = fileName_;
        H = H_;
        L = L_;
        from = from_;
        to = to_;

        noiseType = 3;
        generateTexture();
    }

};

int main(int argc, char **argv)
{
    vec3 greenish = vec3(0.1, 0.2, 0.1);
    vec3 redish = vec3(0.2, 0.1, 0.1);

    Texture texture = Texture(true, true, 16);
    // texture.generateMarble("marble.tex", 3, 3, 5, {white, white * 0.8f, black});
    // texture.generateMarble("marble1.tex", 1, 0, 1, {white, white * 0.8f, white, white * 0.2f});
    // texture.generateMarble("marble2.tex", 1, 0, 3, {white, white * 0.8f, white * 0.2f});

    /* light jade */
    texture.generateLTurbulence("light_jade0.tex", 95, 0.5, 0.5, 0.8);
    texture.generateLTurbulence("light_jade1.tex",  85, 0.5, 0.5, 0.8);
    texture.generateLTurbulence("light_jade2.tex", 75, 0.5, 0.5, 0.8);
    texture.generateLTurbulence("light_jade3.tex", 65, 0.5, 0.5, 0.8);

    // toronto marble
    // vector<vec3> colors = {white, 3.0f * redish, 0.8f * white, 2.0f * redish, white};
    // texture.generateMarble("marble4.tex", 2, 0, 3, colors);

    // texture.generateLTurbulence("jade.tex", 120, 0.7, 0.1, 0.5);
    // texture.generateLTurbulence("somestone_huge.tex", 37, 0.2, 0.5, 1);
    // texture.generateLTurbulence("turbulence.tex", 0, 0);
    // texture.generateHTurbulence("hTurbulence.tex", 1, 0.5, 0, 120);
    // texture.generateLTurbulence("lTurbulence.tex", 192, 1, 0, 1);
    // texture.generateSTurbulence("sTurbulence.tex", 192, 0.5, 0, 1);
}