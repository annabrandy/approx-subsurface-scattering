/* standard includes */
#include <iostream>
#include <cstdlib>

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <array>
#include <ctime>

/* GL and glm includes, have to be in /usr/include/ */
#include <GL/glew.h>
#include <GLFW/glfw3.h> // also defines openGL header
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp> // e.g. for perspective matrix
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

/* imgui imports */
#include "build/imgui.h"
#include "build/imgui_impl_glfw.h"
#include "build/imgui_impl_opengl3.h"

using namespace std;
using namespace glm;

/* load images as textures */
// https://learnopengl.com/Getting-started/Textures
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

/* PROGRAM VARIABLES */

/* time: to calculate animations fluently */
double oldTime{0}, newTime{0};

/* debug and rendering options */
bool drawToUVCoords{false};
bool textureMappingAfterBlurring{false};
bool visualizeDepthMap{false};
int animation{0};
bool backgroundIsWhite{false};
int irradianceMapResolution{512};
int depthMapResolution{1024};

int winWidth{800};
int winHeight{600};

/* This object is the one which should be moved and whose lighting constants are adapted */
int currentObject{0};

/* variables to control mouse movement: used to rotate object around x and y 
    axis */
double mousePosX{0}, mousePosY{0};
bool moveObject{false};

/* Shader programs */
GLuint spDefault, spVisualizeDepth, spTsd, spApplyTxt;

/** Holds the three properties of a vertex: position, normal and UV-Coordinates.
    Earlier version had color, but instead uniform color or texture mapping used.
 */
struct Vertex
{
    vec3 position{0};
    vec3 normal{0};
    vec2 uvCoords{0};

    /* normal optional for quads */
    Vertex(vec3 position_, vec2 uvCoords_, vec3 normal_ = vec3(0, 0, 0))
        : position(position_), normal(normal_), uvCoords(uvCoords_) {}
};

/** overloaded bind functions to bind mat4, vec3 and other types
    used in structs which are bound to shader programs. */
void bind(GLuint uniform, mat4 matrix)
{
    glUniformMatrix4fv(uniform, 1, GL_FALSE, value_ptr(matrix));
}
void bind(GLuint uniform, GLfloat factor)
{
    glUniform1f(uniform, factor);
}
void bind(GLuint uniform, vec2 vector)
{
    glUniform2fv(uniform, 1, value_ptr(vector));
}
void bind(GLuint uniform, vec3 vector)
{
    glUniform3fv(uniform, 1, value_ptr(vector));
}
void bind(GLuint uniform, vec4 vector)
{
    glUniform4fv(uniform, 1, value_ptr(vector));
}
void bind(GLuint uniform, int number)
{
    glUniform1i(uniform, number);
}

vec3 sphericalToEuklidean(vec3 spherical)
{
    return vec3(
               sin(spherical.x) * cos(spherical.y),
               sin(spherical.y),
               cos(spherical.x) * cos(spherical.y)) *
           spherical.z;
}

class Camera
{
    mat4 perspectiveMatrix;

public:
    vec3 position{0};
    vec3 viewDirection{0};
    vec3 upVector{0};

    Camera(){};

    Camera(float fovy, float winWidth, float winHeight, float nearPlane, float farPlane)
    {
        float aspect = winWidth / winHeight;
        perspectiveMatrix = glm::perspective(fovy, aspect, nearPlane, farPlane);
    }

    void initView(vec3 position_, vec3 viewDirection_, vec3 upVector_)
    {
        position = position_;
        viewDirection = viewDirection_;
        upVector = upVector_;
    }

    mat4 getViewMatrix()
    {
        return glm::lookAt(position, viewDirection, upVector);
    }

    mat4 getPerspectiveMatrix()
    {
        return perspectiveMatrix;
    }

} camera;

class Light
{
    mat4 perspectiveMatrix{0};
    mat4 orthographicMatrix{0};

public:
    vec3 sphericalPosition{0};
    vec3 viewDirection{0};
    vec3 upVector{0};

    vec3 color{1};

    // 0 directional, 1 point light
    int type{1};

    int uniform = 30;

    Light() {}

    Light(int i, float fovy, float winWidth, float winHeight,
          float nearPlane, float farPlane)
    {
        uniform += i * 3;

        float aspect = winWidth / winHeight;
        perspectiveMatrix = glm::perspective(fovy, aspect, nearPlane, farPlane);

        float orthoFactor = 130;
        float right = -winWidth / orthoFactor;
        float left = winWidth / orthoFactor;
        float bottom = -winHeight / orthoFactor;
        float top = winHeight / orthoFactor;
        orthographicMatrix = glm::ortho(right, left, bottom, top, nearPlane, farPlane);
    }

    void initView(vec3 sphericalPosition_, vec3 viewDirection_, vec3 upVector_)
    {
        sphericalPosition = sphericalPosition_;
        viewDirection = viewDirection_;
        upVector = upVector_;
    }

    mat4 getViewMatrix()
    {
        return glm::lookAt(sphericalToEuklidean(sphericalPosition),
                           viewDirection, upVector);
    }

    mat4 getProjectionMatrix()
    {
        switch (type)
        {
        case 0:
            return orthographicMatrix;
        case 1:
            return perspectiveMatrix;
        default:
            return mat4{0};
        }
    }

    void bindLight()
    {
        int tmp = uniform;
        bind(tmp++, sphericalToEuklidean(sphericalPosition));
        bind(tmp++, color);
        bind(tmp++, type);
    }
};

vector<Light> lights;

class Texture
{
    vector<GLuint> size{1024, 1024};
    int generateDepthBuffer();
    bool generateFrameBuffer(bool secondTexture = false);

public:
    GLuint texture, texture2, fbo;
    Texture(){};

    void generateTexture(bool secondTexture = false);

    void setSize(GLuint x, GLuint y) { size = {x, y}; };
    bool init(bool twoTextures = false);
    void bindFbo();
    void bindTexture(int x, bool secondTexture = false);
    void setViewport();

} depthMapBuffer;

vector<GLuint> textures2d;
vector<GLuint> textures3d;

/* struct Material

This struct contains all values needed for the calculation of 
Phong lighting, wrap lighting and the transmission of light. */
struct Material
{
    int uniform = 40;
    string name = "default";

    // color of material
    vec3 color{1, 1, 1};

    // lighting model parameters
    float kd{0.5};
    float ks0{0};
    float ks1{0.25};
    float shininess{40};
    float roughnessOn{0.2};
    float roughnessCt{0.3};
    float fresnelCt{0.2};

    int specularModel{2};

    // wrap lighting
    float wrap{0.2};
    float scatterWidth{0};
    vec3 scatterColor{0.3, 0, 0};

    // transmission
    float kt{0.2};
    int absorptionFunction{0};
    vec3 absorptionParameter{10};
    vec3 transmissionColor{1};
    float alpha1{0};
    float alpha2{0};
    float alpha3{0};

    // filter
    int filterSize{2};
    vec3 sigma{1};

    // others
    bool shadowMappingEnabled{true};
    int textureMappingNo{2};

    // debugging values for shadow mapping
    float growVertexAlongNormalFactor{0.019};
    float shadowMappingTreshold{0.004};

    // others, not bound
    int textureNo2d{0};
    int textureNo3d{2};

    Material() {}

    /* construct Material from csv line */
    Material(string csvLine)
    {
        vector<string> csvEntries;
        stringstream stringStream(csvLine);
        string item;

        while (getline(stringStream, item, ';'))
        {
            csvEntries.push_back(item);
        }

        int tmp = 0;
        name = csvEntries[tmp++];
        color.x = stof(csvEntries[tmp++]);
        color.y = stof(csvEntries[tmp++]);
        color.z = stof(csvEntries[tmp++]);

        kd = stof(csvEntries[tmp++]);
        ks0 = stof(csvEntries[tmp++]);
        ks1 = stof(csvEntries[tmp++]);
        shininess = stof(csvEntries[tmp++]);
        roughnessOn = stof(csvEntries[tmp++]);
        roughnessCt = stof(csvEntries[tmp++]);
        fresnelCt = stof(csvEntries[tmp++]);

        specularModel = stoi(csvEntries[tmp++]);

        wrap = stof(csvEntries[tmp++]);
        scatterWidth = stof(csvEntries[tmp++]);
        scatterColor.x = stof(csvEntries[tmp++]);
        scatterColor.y = stof(csvEntries[tmp++]);
        scatterColor.z = stof(csvEntries[tmp++]);

        kt = stof(csvEntries[tmp++]);
        absorptionFunction = stoi(csvEntries[tmp++]);
        absorptionParameter.x = stof(csvEntries[tmp++]);
        absorptionParameter.y = stof(csvEntries[tmp++]);
        absorptionParameter.z = stof(csvEntries[tmp++]);

        transmissionColor.x = stof(csvEntries[tmp++]);
        transmissionColor.y = stof(csvEntries[tmp++]);
        transmissionColor.z = stof(csvEntries[tmp++]);
        alpha1 = stof(csvEntries[tmp++]);
        alpha2 = stof(csvEntries[tmp++]);
        alpha3 = stof(csvEntries[tmp++]);

        filterSize = stoi(csvEntries[tmp++]);
        sigma.x = stof(csvEntries[tmp++]);
        sigma.y = stof(csvEntries[tmp++]);
        sigma.z = stof(csvEntries[tmp++]);

        shadowMappingEnabled = stoi(csvEntries[tmp++]);
        textureMappingNo = stoi(csvEntries[tmp++]);

        growVertexAlongNormalFactor = stof(csvEntries[tmp++]);
        shadowMappingTreshold = stof(csvEntries[tmp++]);

        textureNo2d = stoi(csvEntries[tmp++]);
        textureNo3d = stoi(csvEntries[tmp++]);
    }

    string getCsvLine()
    {
        vector<string> csvEntries;
        csvEntries.push_back(name);

        csvEntries.push_back(to_string(color.x));
        csvEntries.push_back(to_string(color.y));
        csvEntries.push_back(to_string(color.z));

        csvEntries.push_back(to_string(kd));
        csvEntries.push_back(to_string(ks0));
        csvEntries.push_back(to_string(ks1));
        csvEntries.push_back(to_string(shininess));
        csvEntries.push_back(to_string(roughnessOn));
        csvEntries.push_back(to_string(roughnessCt));
        csvEntries.push_back(to_string(fresnelCt));

        csvEntries.push_back(to_string(specularModel));

        csvEntries.push_back(to_string(wrap));
        csvEntries.push_back(to_string(scatterWidth));
        csvEntries.push_back(to_string(scatterColor.x));
        csvEntries.push_back(to_string(scatterColor.y));
        csvEntries.push_back(to_string(scatterColor.z));

        csvEntries.push_back(to_string(kt));
        csvEntries.push_back(to_string(absorptionFunction));

        csvEntries.push_back(to_string(absorptionParameter.x));
        csvEntries.push_back(to_string(absorptionParameter.y));
        csvEntries.push_back(to_string(absorptionParameter.z));

        csvEntries.push_back(to_string(transmissionColor.x));
        csvEntries.push_back(to_string(transmissionColor.y));
        csvEntries.push_back(to_string(transmissionColor.z));
        csvEntries.push_back(to_string(alpha1));
        csvEntries.push_back(to_string(alpha2));
        csvEntries.push_back(to_string(alpha3));

        csvEntries.push_back(to_string(filterSize));
        csvEntries.push_back(to_string(sigma.x));
        csvEntries.push_back(to_string(sigma.y));
        csvEntries.push_back(to_string(sigma.z));

        csvEntries.push_back(to_string(shadowMappingEnabled));
        csvEntries.push_back(to_string(textureMappingNo));

        csvEntries.push_back(to_string(growVertexAlongNormalFactor));
        csvEntries.push_back(to_string(shadowMappingTreshold));

        csvEntries.push_back(to_string(textureNo2d));
        csvEntries.push_back(to_string(textureNo3d));

        string delimiter{";"};
        stringstream csvLine;

        for (int i = 0; i < csvEntries.size(); i++)
        {
            csvLine << csvEntries[i];
            csvLine << delimiter;
        }

        return csvLine.str();
    }

    void bindMaterial()
    {
        int tmp = uniform;
        bind(tmp++, color);

        bind(tmp++, kd);
        bind(tmp++, ks0);
        bind(tmp++, ks1);
        bind(tmp++, shininess);
        bind(tmp++, roughnessOn);
        bind(tmp++, roughnessCt);
        bind(tmp++, fresnelCt);
        bind(tmp++, specularModel);

        bind(tmp++, wrap);
        bind(tmp++, scatterWidth);
        bind(tmp++, scatterColor);

        bind(tmp++, kt);
        bind(tmp++, absorptionFunction);
        bind(tmp++, absorptionParameter);
        bind(tmp++, transmissionColor);
        bind(tmp++, alpha1);
        bind(tmp++, alpha2);
        bind(tmp++, alpha3);

        bind(tmp++, filterSize);
        bind(tmp++, sigma);

        bind(tmp++, shadowMappingEnabled);
        bind(tmp++, textureMappingNo);

        bind(tmp++, growVertexAlongNormalFactor);
        bind(tmp++, shadowMappingTreshold);
    }
};

vector<Material> materials;

class Object
{
    mat4 ModelMatrix{1};
    int numVertices{0};
    int numFaces{0}; // for efficiency tests

public:
    GLuint verticesBuffer{0};

    Texture renderingMap = Texture();
    Material material = Material();

    const char *fileName{""};

    Object(){};

    void scale(vec3 s) { ModelMatrix = glm::scale(ModelMatrix, s); }
    void scale(double s) { scale(vec3(s, s, s)); }
    void rotate(mat4 rot) { ModelMatrix = rot * ModelMatrix; }
    void translate(vec3 translation) { ModelMatrix = glm::translate(ModelMatrix, translation); }
    void setToPosition(vec3 position)
    {
        ModelMatrix = glm::translate(mat4(1), position);
        scale(0.5);
    }

    mat4 getModelMatrix() { return ModelMatrix; }
    int getNumVertices() { return numVertices; }
    int getNumFaces() { return numFaces; }

    bool setupBuffers();
};

vector<Object> objects;

/* partly from: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/ */
bool loadObj(const char *fileName, vector<Vertex> &vertices, int &numVertices, int &numFaces)
{
    FILE *file = fopen(fileName, "r");
    if (!file)
    {
        cerr << "can't read obj file " << fileName << endl;
        return false;
    }

    // define temporal vectors where v, vt and vn from file are stored as one vec3 per line
    vector<vec3> inPositions;
    vector<vec2> inUvCoords;
    vector<vec3> inNormals;

    // define temporal vectors where the indices given by the faces are stored
    vector<unsigned int> positionIndices, uvCoordsIndices, normalIndices;

    // count faces for test
    int facesCounter = 0;

    // read file line per line until EOF
    while (true)
    {
        char firstWord[128];
        if (fscanf(file, "%s", firstWord) == EOF)
        {
            break;
        }

        // parse remaining elements of line depending of first word
        if (strcmp(firstWord, "v") == 0)
        {
            // store to tmpVertices
            vec3 position;
            fscanf(file, "%f %f %f/n", &position.x, &position.y, &position.z);
            inPositions.push_back(position);
        }
        else if (strcmp(firstWord, "vt") == 0)
        {
            // store to tmp uvCoords
            vec2 uvCoords;
            fscanf(file, "%f %f/n", &uvCoords.x, &uvCoords.y);
            inUvCoords.push_back(uvCoords);
        }
        else if (strcmp(firstWord, "vn") == 0)
        {
            // store to tmpNormals
            vec3 normal;
            fscanf(file, "%f %f %f/n", &normal.x, &normal.y, &normal.z);
            inNormals.push_back(normal);
        }
        else if (strcmp(firstWord, "f") == 0)
        {
            facesCounter++;

            // store indices of each face to indices lists
            unsigned int positionIndex[3], uvCoordsIndex[3], normalIndex[3];
            fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
                   &positionIndex[0], &uvCoordsIndex[0], &normalIndex[0],
                   &positionIndex[1], &uvCoordsIndex[1], &normalIndex[1],
                   &positionIndex[2], &uvCoordsIndex[2], &normalIndex[2]);

            // wavefront files start with the index 0
            positionIndices.push_back(positionIndex[0] - 1);
            positionIndices.push_back(positionIndex[1] - 1);
            positionIndices.push_back(positionIndex[2] - 1);
            uvCoordsIndices.push_back(uvCoordsIndex[0] - 1);
            uvCoordsIndices.push_back(uvCoordsIndex[1] - 1);
            uvCoordsIndices.push_back(uvCoordsIndex[2] - 1);
            normalIndices.push_back(normalIndex[0] - 1);
            normalIndices.push_back(normalIndex[1] - 1);
            normalIndices.push_back(normalIndex[2] - 1);
        }
        else
        {
            // ignore other information in .obj file like #, o, s or mtl
        }
    }

    numVertices = positionIndices.size();
    numFaces = facesCounter;
    vertices.clear();

    // then: add all positions, uvCoords and normals at indices referred to from faces to corresponding list

    for (int i = 0; i < numVertices; i++)
    {
        Vertex vertex{
            inPositions[positionIndices[i]],
            inUvCoords[uvCoordsIndices[i]],
            inNormals[normalIndices[i]]};

        vertices.push_back(vertex);
    }

    fclose(file);
    return true;
}

template <typename T>
void generateArrayBuffer(GLuint &bufferObject, vector<T> &bufferData,
                         GLenum target = GL_ARRAY_BUFFER)
{
    glGenBuffers(1, &bufferObject);
    glBindBuffer(target, bufferObject);
    glBufferData(target, bufferData.size() * sizeof(T), &bufferData[0],
                 GL_STATIC_DRAW);
}

bool Object::setupBuffers()
{
    vector<Vertex> vertices;

    string totalFileName = "./models/" + string(fileName) + ".obj";

    if (!loadObj(totalFileName.c_str(), vertices, numVertices, numFaces))
    {
        cerr << "error loading object file" << endl;
        return false;
    }

    generateArrayBuffer(verticesBuffer, vertices);

    return true;
}

void Texture::generateTexture(bool secondTexture)
{
    if (secondTexture)
    {
        glDeleteTextures(1, &texture2);
        glGenTextures(1, &texture2);
        glBindTexture(GL_TEXTURE_2D, texture2);
    }
    else
    {
        glDeleteTextures(1, &texture);
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    GLint internalFormat = GL_RGBA16F;
    GLenum format = GL_RGBA;
    GLenum type = GL_FLOAT;

    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        internalFormat,
        size[0],
        size[1],
        0,
        format,
        type,
        NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);

    glBindTexture(GL_TEXTURE_2D, 0);
}

int Texture::generateDepthBuffer()
{
    GLuint depthBuffer;
    glDeleteRenderbuffers(1, &depthBuffer);
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);

    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size[0], size[1]);
    return depthBuffer;
}

bool Texture::generateFrameBuffer(bool secondTexture)
{
    glDeleteFramebuffers(1, &fbo);
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    if (secondTexture)
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texture2, 0);
    }

    GLuint depthBuffer = generateDepthBuffer();
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        cerr << "framebuffer not ok" << endl;
        return false;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return true;
}

bool Texture::init(bool twoTextures)
{
    generateTexture();
    if (twoTextures)
    {
        generateTexture(true);
        return generateFrameBuffer(true);
    }
    return generateFrameBuffer();
}

void Texture::bindFbo()
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void Texture::bindTexture(int x, bool secondTexture)
{
    glActiveTexture(GL_TEXTURE0 + x);
    if (secondTexture)
    {
        glBindTexture(GL_TEXTURE_2D, texture2);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, texture);
    }
}

void Texture::setViewport()
{
    glViewport(0, 0, size[0], size[1]);
}

/** CALLBACKS */

/* Error callback function for GLFW */
void errorCallback(int error, const char *description)
{
    cerr << "Error: " << description;
}

/* Called immediately after close flag, which glfwWindowShouldClose() checks, was set */
void windowCloseCallback(GLFWwindow *window)
{
    /* manually set the flag that the window should be closed */
    cout << "[x] clicked, preparing to close window" << endl;
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

/* Receives key action, like pressed or released */
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        cout << "ESC pressed, preparing to close window" << endl;
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

/* Receives how much scrolled. With mouse just 1 od -1 in y direction, with trackpad also in x or both */
void scrollCallback(GLFWwindow *window, double xOffset, double yOffset)
{
    if (!ImGui::IsAnyWindowHovered() && yOffset != 0)
    {
        float diff = 0.3;
        float s = yOffset * (yOffset > 0 ? (1 + diff) : -(1 - diff));
        objects[currentObject].scale(s);
    }
}

/* receives button (which mouse button) and action (pressed or released) */
void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        moveObject = (action == GLFW_PRESS) && !ImGui::IsAnyWindowHovered();
    }
}

/* arcball implementation found somewhere online */

// helper vor arc ball
vec3 projectOnSphere(double xPos, double yPos, double zPos)
{
    // arcball approach

    // create an imaginative sphere
    float radius = 500;              // in pixels
    vec3 center = vec3(400, 300, 0); // in pixels
    vec3 p = vec3(xPos, yPos, zPos) - center;

    // flip y axis
    p = vec3(p.x, -p.y, p.z);

    float radius2 = radius * radius;
    float length2 = p.x * p.x + p.y * p.y;

    if (length2 <= radius2)
    {
        p.z = sqrt(radius2 - length2);
    }

    else
    {
        p.x *= radius / sqrt(length2);
        p.y *= radius / sqrt(length2);
        p.z = 0;
    }

    return glm::normalize(p); // normalize
}

/** When the position of the mouse changes */
void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos)
{
    if (moveObject)
    {
        // change model matrix
        float deltaX = xPos - mousePosX;
        float deltaY = yPos - mousePosY;

        float velocity = 0.3;

        // try to do stuff
        vec3 anchorPos = projectOnSphere(mousePosX, mousePosY, 0);
        vec3 currentPos = projectOnSphere(xPos, yPos, 0);

        // calc quaternion

        // the axis to turn around
        vec3 axis = glm::cross(anchorPos, currentPos);

        // the angle to turn around
        float dot = glm::dot(anchorPos, currentPos);
        float angle = glm::clamp(acosf(dot) * 500, 0.0f, 1.0f);

        quat myQuaternion = glm::angleAxis(angle, axis);
        // normalize quat
        glm::normalize(myQuaternion);
        mat4 rot = glm::toMat4(myQuaternion);
        objects[currentObject].rotate(rot);
    }

    mousePosX = xPos;
    mousePosY = yPos;
}

bool setupWindowAndGlfw(GLFWwindow *&window, int width, int height,
                        const char *title, bool runTestCase)
{
    /* initialize GLFW library, returns GLFW_TRUE or GLFW_FALSE */
    if (!glfwInit())
    {
        cerr << "error: GLFW initialization failed" << endl;
        return false;
    }

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode *mode = glfwGetVideoMode(monitor);

    /* create window and openGL context: returns NULL if unsuccessful */
    window = glfwCreateWindow(width + 400, height, title, NULL, NULL);

    if (!window)
    {
        cerr << "error: GLFW window initialization failed" << endl;
        return false;
    }

    /* set functions, which handle events */
    glfwSetErrorCallback(errorCallback);
    glfwSetWindowCloseCallback(window, windowCloseCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetScrollCallback(window, scrollCallback);

    /* make the OpenGL context (the window) the current context */
    glfwMakeContextCurrent(window);

    if (runTestCase)
        glfwSwapInterval(0); // for fps measurement
    else
        glfwSwapInterval(1); // for normal rendering

    /* initialize GLEW (GL extension wrangler) */
    GLenum res = glewInit();
    if (res != GLEW_OK)
    {
        cerr << "error: glew initialization failed \n"
             << glewGetErrorString(res) << endl;
        return false;
    }

    return true;
}

bool addShader(GLuint handler, string fileName,
               string shaderInclude, string shaderHeader, GLenum shaderType)
{
    /* read shader source code from the specified filename */
    ifstream ifs1(shaderHeader);
    string content1((istreambuf_iterator<char>(ifs1)),
                    (istreambuf_iterator<char>()));

    ifstream ifs2(shaderInclude);
    string content2((istreambuf_iterator<char>(ifs2)),
                    (istreambuf_iterator<char>()));

    ifstream ifs3(fileName);
    string content3((istreambuf_iterator<char>(ifs3)),
                    (istreambuf_iterator<char>()));

    const char *shaderSource[3] = {content1.c_str(), content2.c_str(), content3.c_str()};

    /* create shader and put it together with source string */
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 3, shaderSource, NULL);

    /* compile shader and check whether it was successful (there might be programming errors in it) */
    int success;
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (!success)
    {
        char infoLog[512];
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        cerr << "error: shading compilation failed " << fileName << endl
             << infoLog << endl;
        return false;
    }

    /* attach shader to shader program */
    glAttachShader(handler, shader);

    /* delete shader, not need anymore after being bound to */
    glDeleteShader(shader);

    return true;
}

GLuint setupShaderProgram(string vsName, string fsName, string sHeader, string vsHeader, string fsHeader)
{
    /* create shader proram and attach both shaders to it */
    GLuint id = glCreateProgram();
    addShader(id, vsName, vsHeader, sHeader, GL_VERTEX_SHADER);
    addShader(id, fsName, fsHeader, sHeader, GL_FRAGMENT_SHADER);

    /* variables for success/failure information of linking and validation */
    int success;
    char infoLog[512];

    /* link program and check whether linking worked */
    glLinkProgram(id);
    glGetProgramiv(id, GL_LINK_STATUS, &success);

    if (!success)
    {
        glGetProgramInfoLog(id, 512, NULL, infoLog);
        cerr << "error: linking shader program failed " << vsName << "\n"
             << infoLog << endl;
        return 0;
    }

    /* fixes sampler3D problem */
    glUseProgram(id);
    glUniform1i(0, 0);
    glUniform1i(1, 1);
    glUniform1i(2, 2);
    glUniform1i(3, 3);

    /* validate program */
    glValidateProgram(id);
    glGetProgramiv(id, GL_VALIDATE_STATUS, &success);

    if (!success)
    {
        glGetProgramInfoLog(id, sizeof(infoLog), NULL, infoLog);
        cerr << "error: Invalid shader program vs or fs: " << vsName << endl
             << infoLog << endl;
        return 0;
    }

    return id;
}

/** An enum defining constants to refer to the vertex attributes 
    passed to the vertex shader. */
enum vAttributes
{
    vPosition,
    vNormal,
    vUvCoord,
    numVAttributes
};

void enableVertexDataHelper(GLuint vType, GLuint buffer,
                            GLuint numberPerVertex, int offset)
{
    glEnableVertexAttribArray(vType);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glVertexAttribPointer(vType, numberPerVertex, GL_FLOAT,
                          GL_FALSE, sizeof(Vertex), (void *)offset);
}

void enableVertexData(GLuint verticesBuffer, vector<int> vertexAttributesVector = {vPosition, vNormal, vUvCoord})
{
    for (int i : vertexAttributesVector)
    {
        switch (i)
        {
        case vPosition:
            enableVertexDataHelper(vPosition, verticesBuffer, 3, offsetof(Vertex, position));
            break;
        case vNormal:
            enableVertexDataHelper(vNormal, verticesBuffer, 3, offsetof(Vertex, normal));
            break;
        case vUvCoord:
            enableVertexDataHelper(vUvCoord, verticesBuffer, 2, offsetof(Vertex, uvCoords));
            break;
        default:
            break;
        }
    }
}

void disableVertexData(vector<int> vertexAttributesVector = {vPosition, vNormal, vUvCoord})
{
    for (int i : vertexAttributesVector)
    {
        glDisableVertexAttribArray(i);
    }
}

void clearColor()
{
    glClear(GL_COLOR_BUFFER_BIT);

    if (backgroundIsWhite)
        glClearColor(1, 1, 1, 0);
    else
        glClearColor(0, 0, 0, 0);
}

void drawObject(int i, bool twoColorBuffers = false)
{
    objects[i].material.bindMaterial();

    // calculate and bind matrices
    mat4 ViewMatrix = camera.getViewMatrix();
    mat4 MVMatrix = ViewMatrix * objects[i].getModelMatrix();
    mat4 MVPMatrix = camera.getPerspectiveMatrix() * MVMatrix;
    mat4 NormalMatrix = transpose(inverse(MVMatrix));

    mat4 LightMVMatrix = lights[0].getViewMatrix() * objects[i].getModelMatrix();
    mat4 LightMVPMatrix = lights[0].getProjectionMatrix() * LightMVMatrix;

    int tmp = 4;
    bind(tmp++, ViewMatrix);
    bind(tmp++, MVMatrix);
    bind(tmp++, MVPMatrix);
    bind(tmp++, NormalMatrix);

    bind(tmp++, LightMVMatrix);
    bind(tmp++, LightMVPMatrix);

    enableVertexData(objects[i].verticesBuffer);

    if (twoColorBuffers)
    {
        GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
        glDrawBuffers(2, drawBuffers);
    }

    glDrawArrays(GL_TRIANGLES, 0, objects[i].getNumVertices());

    disableVertexData();
}

void depthmapPass(bool runTestCase)
{
    depthMapBuffer.bindFbo();
    glUseProgram(spDefault);

    bind(14, true);

    glClear(GL_DEPTH_BUFFER_BIT);
    clearColor();

    int startIteration = runTestCase ? 0 : 1;

    for (int i = startIteration; i < objects.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, textures2d[objects[i].material.textureNo2d]);
        glActiveTexture(GL_TEXTURE0 + 3);
        glBindTexture(GL_TEXTURE_3D, textures3d[objects[i].material.textureNo3d]);
        drawObject(i, true);
    }
}

void drawDepthmap(GLuint quadVerticesBuffer)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glUseProgram(spVisualizeDepth);

    depthMapBuffer.bindTexture(0);
    depthMapBuffer.bindTexture(1, true);

    glClear(GL_DEPTH_BUFFER_BIT);
    clearColor();

    enableVertexData(quadVerticesBuffer, {vPosition, vUvCoord});
    glDrawArrays(GL_TRIANGLES, 0, 6);
    disableVertexData({vPosition, vUvCoord});
}

void textureSpaceDIffusion(GLuint quadVerticesBuffer, int i, bool horizontal)
{
    bind(18, horizontal);

    glClear(GL_DEPTH_BUFFER_BIT);

    enableVertexData(quadVerticesBuffer, {vPosition, vUvCoord});
    glDrawArrays(GL_TRIANGLES, 0, 6);
    disableVertexData({vPosition, vUvCoord});
}

void drawScene(GLuint quadVerticesBuffer)
{
    // first: draw all tsd objects to their texture space
    glViewport(0, 0, irradianceMapResolution, irradianceMapResolution);

    for (int i = 0; i < objects.size(); i++)
    {
        bool textureSpaceDiffusionEnabled = objects[i].material.filterSize > 0;

        if (textureSpaceDiffusionEnabled)
        {
            objects[i].renderingMap.bindFbo();

            // draw normally to texture
            glUseProgram(spDefault);

            /* bind light sources to shader programs */
            bind(16, (int)lights.size());
            for (int i = 0; i < lights.size(); i++)
                lights[i].bindLight();

            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, textures2d[objects[i].material.textureNo2d]);

            glActiveTexture(GL_TEXTURE0 + 1);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture);
            glActiveTexture(GL_TEXTURE0 + 2);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture2);

            glActiveTexture(GL_TEXTURE0 + 3);
            glBindTexture(GL_TEXTURE_3D, textures3d[objects[i].material.textureNo3d]);

            bind(13, true);  // enable tsd
            bind(14, false); // enable depth pass

            bind(22, textureMappingAfterBlurring);
            bind(23, drawToUVCoords);

            glClear(GL_DEPTH_BUFFER_BIT);
            clearColor();
            drawObject(i);

            // perform tsd
            glUseProgram(spTsd);

            objects[i].renderingMap.bindFbo();
            objects[i].renderingMap.bindTexture(0);

            objects[i].material.bindMaterial();

            bind(15, irradianceMapResolution);
            bind(22, textureMappingAfterBlurring);
            bind(23, drawToUVCoords);

            textureSpaceDIffusion(quadVerticesBuffer, i, true);
            textureSpaceDIffusion(quadVerticesBuffer, i, false);
        }
    }

    /* second: draw objects directly or with tsd to screen */
    glViewport(0, 0, winWidth, winHeight);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_DEPTH_BUFFER_BIT);
    clearColor();

    for (int i = 0; i < objects.size(); i++)
    {
        if (drawToUVCoords && i != currentObject)
            continue;

        bool textureSpaceDiffusionEnabled = objects[i].material.filterSize > 0;

        if (textureSpaceDiffusionEnabled)
        {
            // draw diffused to screen
            glUseProgram(spApplyTxt);

            objects[i].renderingMap.bindTexture(0);

            glActiveTexture(GL_TEXTURE0 + 1);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture);
            glActiveTexture(GL_TEXTURE0 + 2);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture2);

            glActiveTexture(GL_TEXTURE0 + 3);
            glBindTexture(GL_TEXTURE_3D, textures3d[objects[i].material.textureNo3d]);

            glActiveTexture(GL_TEXTURE0 + 11);
            glBindTexture(GL_TEXTURE_2D, textures2d[objects[i].material.textureNo2d]);

            bind(22, textureMappingAfterBlurring);
            bind(23, drawToUVCoords);

            drawObject(i);
        }
        else
        {
            // draw normally to screen
            glUseProgram(spDefault);

            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, textures2d[objects[i].material.textureNo2d]);

            /* bind light sources to shader programs */
            bind(16, (int)lights.size());
            for (int i = 0; i < lights.size(); i++)
                lights[i].bindLight();

            glActiveTexture(GL_TEXTURE0 + 1);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture);
            glActiveTexture(GL_TEXTURE0 + 2);
            glBindTexture(GL_TEXTURE_2D, depthMapBuffer.texture2);

            glActiveTexture(GL_TEXTURE0 + 3);
            glBindTexture(GL_TEXTURE_3D, textures3d[objects[i].material.textureNo3d]);

            bind(13, false); // enable tsd
            bind(14, false); // enable depth pass

            bind(22, textureMappingAfterBlurring);
            bind(23, drawToUVCoords);

            drawObject(i);
        }
    }
}

/** first function called in main render loop */
void display(GLuint quadVerticesBuffer, bool drawDepthPass, bool runTestCase)
{
    /* bind light sources to shader programs */
    bind(16, (int)lights.size());
    for (int i = 0; i < lights.size(); i++)
        lights[i].bindLight();

    /* draw depth map */
    // if (drawDepthPass || !runTestCase)
    {
        depthMapBuffer.setViewport();
        depthmapPass(runTestCase);
    }
    glViewport(0, 0, winWidth, winHeight);

    if (visualizeDepthMap)
    {
        //depthMap.setViewport();
        drawDepthmap(quadVerticesBuffer);
    }
    else
    {
        drawScene(quadVerticesBuffer);
    }
}

class Test
{
public:
    // measurement
    double maxRuntime{5};
    double runtimeSeconds{0};
    int framesCounter{0};

    vector<string> resultCsvLines;

    // workflow and output
    int caseNumber{0};

    // for all materials ..
    int maxNumberOfMaterials{3};
    int startAtMaterial{3};
    int currentMaterial{startAtMaterial};

    // ... and all iteration levels ...
    int maxNumberOfIterations{6}; // 6
    int currentIteration{0};

    // ... let it run for 10 seconds ...
    // ... and repeat this 6 times
    int maxNumberOfRuns{6};

    bool drawDepthPass{false};

    // test case 1
    const char *fileName{"sphere_my"};
    int meshSizeVertices{0};
    int meshSizeFaces{0};

    // test case 2 and 3
    int numberOfObjects{0};

    // test case 4
    float scaleOfObject{3.5};

    // others, so far not in test cases
    int winHeight{600}, winWidth{800};
    int irradianceMapResolution{512};
    int depthMapResolution{1024};

private:
    Material getMaterial(
        bool pbr, // phong vs other local ill
        bool simTrans,
        bool tsd,
        bool activateAlpha1 = false,
        bool activateAlpha2 = false,
        int filterSize = 1)
    {
        Material material = Material();

        material.kd = 0.5;
        material.ks0 = 0.5;

        if (pbr)
        {
            material.scatterWidth = 0.3;
            material.wrap = 0.3;
            material.roughnessOn = 0.3;
            material.specularModel = 2;
        }
        if (simTrans)
        {
            material.kt = 0.2;
            material.absorptionFunction = 0;
            material.alpha1 = activateAlpha1 ? 0.5 : 0;
            material.alpha2 = activateAlpha2 ? 0.5 : 0;
            material.alpha3 = 0;
        }
        if (tsd)
        {
            material.filterSize = filterSize;
            material.ks1 = 0.5;
        }

        return material;
    }

    Material getMaterialNumber(int materialNumber)
    {
        bool pbr{false}, simTrans{false}, tsd{false};

        bool activateAlpha1 = false;
        bool activateAlpha2 = false;
        int filterSize = 1;

        switch (materialNumber)
        {
        case 0: // Phong
            break;
        case 1: // PBR
            pbr = true;
            break;
        case 2: // SimTrans
            simTrans = true;
            break;
        case 3: // TSD
            tsd = true;
            filterSize = 1;
            break;
        case 4: // all
            pbr = true;
            simTrans = true;
            tsd = true;
            filterSize = 1;
            break;

        // new materials
        case 5:
            simTrans = true;
            activateAlpha1 = true;
            break;
        case 6:
            simTrans = true;
            activateAlpha2 = true;
            break;
        case 7:
            simTrans = true;
            activateAlpha1 = true;
            activateAlpha2 = true;
            break;

        case 8:
            tsd = true;
            filterSize = 2;
            break;
        case 9:
            tsd = true;
            filterSize = 3;
            break;
        case 10:
            tsd = true;
            filterSize = 4;
            break;
        }

        drawDepthPass = simTrans;

        return getMaterial(pbr, simTrans, tsd,
                           activateAlpha1, activateAlpha2, filterSize);
    }

    void setupObjectsHelper(int number, int meshSize, bool offset = false)
    {
        numberOfObjects = number;

        switch (meshSize)
        {
        case 1:
            fileName = "sphere_500";
            break;
        case 2:
            fileName = "sphere_1000";
            break;
        case 3:
            fileName = "sphere_1500";
            break;
        case 4:
            fileName = "sphere_2000";
            break;
        case 5:
            fileName = "sphere_2500";
            break;
        case 6:
            fileName = "sphere_3000";
            break;
        case 7:
            fileName = "sphere_10k";
            break;

        default:
            fileName = "sphere_my";
            break;
        }

        Material material = getMaterialNumber(0);
        objects.clear();

        for (int i = 0; i < number; i++)
        {
            Object object;
            object.fileName = fileName;
            object.material = material;

            object.scale(scaleOfObject);

            if (offset)
            {
                // space for 6 objects
                float offsetX = -2.2 + 2.2f * glm::mod((float)i, 3.0f);
                float offsetY = -1.1 + 2.2 * (i > 2);
                object.translate(vec3(offsetX, offsetY, 0));
            }

            objects.push_back(object);
        }
    }

public:
    Test(int caseNumber_) : caseNumber(caseNumber_) {}

    void setupObjects(int iteration)
    {
        currentIteration = iteration % maxNumberOfIterations;

        camera.initView(vec3(0, 0, 10), vec3(0, 0, 0), vec3(0, 1, 0));
        lights[0].initView(vec3(0, 0, 10), vec3(0, 0, 0), vec3(0, 1, 0));
        lights[0].type = 0;

        switch (caseNumber)
        {
        case 0:
            return;
        case 1: // increase mesh size
            setupObjectsHelper(1, currentIteration + 1);
            return;
        case 2: // increase number of objects, look at them
            scaleOfObject = 1.5;
            setupObjectsHelper(currentIteration + 1, 2, true);
            return;
        case 3: // increase number of objects, look away
            camera.initView(vec3(0, 0, 10), vec3(0, 10, 10), vec3(0, 1, 0));
            scaleOfObject = 1.5;
            setupObjectsHelper(currentIteration + 1, 2, true);
            return;
        case 4: // increase scale of object
            scaleOfObject = 1 + currentIteration * 0.5;
            setupObjectsHelper(1, 2);
            return;
        case 5: // screen size
            setupObjectsHelper(1, 2);
            return;
        case 6: // mesh size 10k
            setupObjectsHelper(1, 7);
            return;
        case 7: // irradiance map resolution
            setupObjectsHelper(1, 2);
            return;

        default:
            return;
        }
    }

    bool nextTestIteration(GLFWwindow *window)
    {
        // later: write to csv. Open file per test case
        printAndStoreResult(window);

        // reset values for next material
        currentMaterial++;
        runtimeSeconds = 0;
        framesCounter = 0;

        lights[0].sphericalPosition = vec3(0, 0, 16);

        if (currentMaterial > maxNumberOfMaterials)
        {
            currentMaterial = startAtMaterial;
            return false;
        }

        // set material
        Material material = getMaterialNumber(currentMaterial);

        for (int i = 0; i < objects.size(); i++)
        {
            objects[i].material = material;
        }

        cout << "... run material " << currentMaterial << endl;

        return true;
    }

    bool getDrawDepthPass()
    {
        return drawDepthPass;
    }

    void printAndStoreResult(GLFWwindow *window)
    {
        std::time_t result = std::time(nullptr);
        string timestamp = string(asctime(localtime(&result)));
        timestamp.pop_back();

        float avgFps = framesCounter / (float)runtimeSeconds;

        vector<string> results;
        results.push_back(timestamp);
        results.push_back(to_string(caseNumber));
        results.push_back(to_string(currentIteration));
        results.push_back(to_string(currentMaterial));
        results.push_back(to_string(avgFps));
        results.push_back(to_string(runtimeSeconds));

        results.push_back(fileName);
        results.push_back(to_string(meshSizeVertices));
        results.push_back(to_string(meshSizeFaces));
        results.push_back(to_string(numberOfObjects));
        results.push_back(to_string(scaleOfObject));

        // real size of framebuffer
        glfwGetFramebufferSize(window, &winWidth, &winHeight);

        results.push_back(to_string(winWidth));
        results.push_back(to_string(winHeight));
        results.push_back(to_string(depthMapResolution));
        results.push_back(to_string(irradianceMapResolution));

        string delimiter{"; "};
        stringstream csvLine;
        csvLine << results[0];

        for (int i = 1; i < results.size(); i++)
        {
            csvLine << delimiter;
            csvLine << results[i];
        }

        resultCsvLines.push_back(csvLine.str());
        cout << csvLine.str() << endl;
    }

    void writeResultsToFile()
    {
        ofstream file;
        file.open("./efficiency.csv", ios::app);

        for (int i = 0; i < resultCsvLines.size(); i++)
        {
            file << resultCsvLines[i] << endl;
        }
        file.close();
    }
};

/** second function called in main render loop.
    Currently simply performs rotation */
void onIdle(bool runTestCase, Test &test)
{
    double delta = newTime - oldTime;

    switch (animation)
    {
    case 0:
        break;

    case 1:
        lights[0].sphericalPosition[0] += delta * 0.5;
        if (lights[0].sphericalPosition[0] > M_PI)
            lights[0].sphericalPosition[0] = -M_PI;
        break;

    default:
        break;
    }
}

/* from: https://stackoverflow.com/questions/5844858/how-to-take-screenshot-in-opengl */
void saveScreenshotToFile(std::string filename, int windowWidth, int windowHeight)
{
    const int numberOfPixels = windowWidth * windowHeight * 3;
    unsigned char pixels[numberOfPixels];

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadBuffer(GL_FRONT);
    glReadPixels(0, 0, windowWidth, windowHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, pixels);

    FILE *outputFile = fopen(filename.c_str(), "w");
    short header[] = {0, 2, 0, 0, 0, 0, (short)windowWidth, (short)windowHeight, 24};

    fwrite(&header, sizeof(header), 1, outputFile);
    fwrite(pixels, numberOfPixels, 1, outputFile);
    fclose(outputFile);

    cout << "*** Finish writing to file " << filename << endl;
}

GLuint generate2dTexture(string fileName)
{
    /* read data from file */
    int width, height, nrChannels;
    string totalFileName = "data/" + fileName;
    unsigned char *data = stbi_load(totalFileName.c_str(), &width, &height, &nrChannels, 0);

    if (!data)
    {
        cout << "error with reading file: " << totalFileName << "!" << endl;
        return 0;
    }

    /* generate texture */
    GLenum glenum = GL_TEXTURE_2D;
    GLuint texture2d;

    glDeleteTextures(1, &texture2d);
    glGenTextures(1, &texture2d);
    glBindTexture(glenum, texture2d);

    GLint internalFormat = GL_RGB;
    GLenum format = GL_RGB;
    GLenum type = GL_UNSIGNED_BYTE;

    // fixes reading
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexImage2D(
        glenum,
        0,
        internalFormat,
        width,
        height,
        0,
        format,
        type,
        data);

    glTexParameteri(glenum, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(glenum, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(glenum, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(glenum, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(glenum, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(glenum, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);

    glBindTexture(glenum, 0);
    stbi_image_free(data);
    return texture2d;
}

GLuint generate3dTexture(const char *fileName)
{
    /* generate texture */
    GLenum glenum = GL_TEXTURE_3D;

    GLuint texture3d;
    glDeleteTextures(1, &texture3d);
    glGenTextures(1, &texture3d);
    glBindTexture(glenum, texture3d);

    /* read data */
    GLint textureSize = 128;
    GLubyte *data = new GLubyte[3 * textureSize * textureSize * textureSize];

    ifstream file;
    file.open("data/" + string(fileName));

    // read file line per line
    string line;
    vector<string> lines;
    int counter = 0;

    while (getline(file, line))
    {
        stringstream lineStream(line);
        string lineItem;
        while (getline(lineStream, lineItem, ' '))
        {
            data[counter] = stoi(lineItem);
            counter++;
        }
    }
    file.close();

    GLint internalFormat = GL_RGB;
    GLenum format = GL_RGB;
    GLenum type = GL_UNSIGNED_BYTE;

    glTexImage3D(
        glenum,
        0,
        internalFormat,
        textureSize,
        textureSize,
        textureSize,
        0,
        format,
        type,
        data);

    glTexParameteri(glenum, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(glenum, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(glenum, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);

    glTexParameteri(glenum, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(glenum, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(glenum, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(glenum, GL_TEXTURE_COMPARE_FUNC, GL_ALWAYS);

    glBindTexture(glenum, 0);
    return texture3d;
}

bool setupShaderPrograms()
{
    /* shader programs */
    string sHeader = "shaders/header.shader";
    string vsHeader = "shaders/header.vs";
    string fsHeader = "shaders/header.fs";

    spDefault = setupShaderProgram("shaders/default.vs", "shaders/default.fs", sHeader, vsHeader, fsHeader);
    spApplyTxt = setupShaderProgram("shaders/applyTexture.vs",
                                    "shaders/applyTexture.fs", sHeader, vsHeader, fsHeader);
    spTsd = setupShaderProgram("shaders/applyTextureSpaceDiff.vs",
                               "shaders/applyTextureSpaceDiffCalc.fs", sHeader, vsHeader, fsHeader);
    spVisualizeDepth = setupShaderProgram("shaders/visualizeDepthmap.vs",
                                          "shaders/visualizeDepthmap.fs", sHeader, vsHeader, fsHeader);

    return (spDefault && spApplyTxt && spTsd && spVisualizeDepth);
}


void create1HeadScene()
{
    Object sphere;
    sphere.fileName = "bunny_downloaded_org";
    sphere.material.color = vec3(1, 1, 1);
    // sphere.material.kd = 0.5;
    // sphere.material.ks1 = 0.5;

    sphere.scale(1.5);
    objects.push_back(sphere);
}

void addFloorToScene()
{
    Object floor;
    floor.fileName = "squad_my";
    floor.material.color = vec3(0.5);
    floor.material.kd = 0.5;
    floor.material.textureMappingNo = 0;
    floor.material.filterSize = 0;

    floor.scale(vec3(10, 1, 10));
    floor.translate(vec3(0, -1, 0));
    objects.push_back(floor);
}

bool setupScene(bool runTestCase)
{
    /* camera and light */
    float nearPlane{1};
    float farPlane{20};

    camera = Camera(radians(45.0f), winWidth, winHeight, nearPlane, farPlane);
    camera.initView(vec3(0, (float)M_PI / 4.0f, 4.0f), vec3(0, 0, 0), vec3(0, 1, 0));

    /* primary light source, able to case shadows and simulate transmission */
    Light light0 = Light(0, radians(45.0f), winWidth, winHeight, nearPlane, farPlane);
    light0.initView(vec3(-1.0f, 0, 10), vec3(0, 0, 0), vec3(0, 1, 0));
    lights.push_back(light0);

    /* Optional second light source */
    if (true && !runTestCase)
    {
        Light light1 = Light(1, radians(45.0f), winWidth, winHeight, nearPlane, farPlane);
        light1.initView(vec3(1, 0, 10), vec3(0, 0, 0), vec3(0, 1, 0));
        light1.color = vec3(0);
        lights.push_back(light1);

        // Light light2 = Light(2, radians(45.0f), winWidth, winHeight, nearPlane, farPlane);
        // light2.initView(vec3(1, 0, 10), vec3(0, 0, 0), vec3(0, 1, 0));
        // lights.push_back(light2);
    }

    return true;
}

bool setupObjects(bool runTestCase, Test &test)
{
    if (!runTestCase)
    {
        Object lightObject;
        lightObject.fileName = "sphere_my";
        lightObject.material.filterSize = 0;
        objects.push_back(lightObject);

        currentObject = 1;
        // addFloorToScene();
        create1HeadScene();
    }

    /* setup textures and buffers */
    int renderMapSize = irradianceMapResolution;
    for (int i = 0; i < objects.size(); i++)
    {
        if (!objects[i].setupBuffers())
        {
            cout << "error setting up buffers" << endl;
            return false;
        }

        objects[i].renderingMap.setSize(renderMapSize, renderMapSize);
        if (!objects[i].renderingMap.init())
        {
            cerr << "texture init failure" << endl;
            return false;
        }

        if (runTestCase)
        {
            test.meshSizeFaces = objects[i].getNumFaces();
            test.meshSizeVertices = objects[i].getNumVertices();
        }
    }

    return true;
}

void setupImgui(GLFWwindow *window)
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    // enable keyboard controls?
    ImGui::StyleColorsDark();
    // platform and renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 130");
}

void setupMaterials()
{
    /* read csv file and store materials */
    ifstream file;
    file.open("materials.csv");
    string line;

    while (getline(file, line))
    {
        if (line.length() < 3)
            continue;
        materials.push_back(Material(line));
    }
    file.close();

    if (materials.size() <= 0)
        materials.push_back(Material());
    cout << "end of function" << endl;
}

void writeMaterials()
{
    ofstream file;
    file.open("materials.csv");

    for (int i = 0; i < materials.size(); i++)
    {
        file << materials[i].getCsvLine() << endl;
    }
    file.close();
}

/* Initializes the GLFW context and window, the MVP matrices and a shader program, starts render loop */
int main(void)
{
    cout << "Hello :)" << endl;
    const char *windowName = "Project";
    int drawDepthPass = true;

    /* setup test */
    int runTestCase = 0;
    Test test = Test(runTestCase);

    winWidth = 800;
    winHeight = 650;

    if (runTestCase)
    {
        windowName = "AUTOMATIC TEST RUNNING";

        irradianceMapResolution = test.irradianceMapResolution;
        depthMapResolution = test.depthMapResolution;

        winWidth = test.winWidth;
        winHeight = test.winHeight;
    }

    GLFWwindow *window;
    if (!setupWindowAndGlfw(window, winWidth, winHeight, windowName, runTestCase))
        return EXIT_FAILURE;
    else
        cout << "> done setting up window" << endl;

    setupImgui(window);
    glEnable(GL_DEPTH_TEST);

    if (!setupShaderPrograms())
        return EXIT_FAILURE;
    else
        cout << "> done setting up shader programs" << endl;

    /* Materials */
    setupMaterials();
    writeMaterials();

    /* store material names for GUI */
    char **materialNamesList = new char *[128];

    for (int i = 0; i < materials.size(); i++)
    {
        materialNamesList[i] = new char[materials[i].name.size() + 1];
        strcpy(materialNamesList[i], materials[i].name.c_str());
        cout << "load material " << materials[i].name.c_str() << endl;
    }

    cout << "> done setting up materials" << endl;

    depthMapBuffer.setSize(depthMapResolution, depthMapResolution);
    if (!depthMapBuffer.init(true))
    {
        cerr << "texture init failure" << endl;
        return EXIT_FAILURE;
    }
    else
        cout << "done setting up depth map buffer" << endl;

    /* textures: keep at least one */
    const char *textures2dList[] = {"uvtemplate.bmp"};

    for (int i = 0; i < IM_ARRAYSIZE(textures2dList); i++)
    {
        cout << "init 2D texture " << textures2dList[i] << endl;
        textures2d.push_back(generate2dTexture(textures2dList[i]));
    }

    const char *textures3dList[] = {
        "marble.tex", "light_jade0.tex", "marble4.tex"
    };

    for (int i = 0; i < IM_ARRAYSIZE(textures3dList); i++)
    {
        cout << "init 3D texture " << textures3dList[i] << endl;
        textures3d.push_back(generate3dTexture(textures3dList[i]));
    }

    /* initialize buffer and data for depth map visualization */
    GLuint quadVerticesBuffer;
    float s = 1.0f;
    float d = 0.0f;
    vector<Vertex> quadVertices{
        Vertex{vec3(-s, -s, d), vec2(0.0f, 0.0f)},
        Vertex{vec3(-s, s, d), vec2(0.0f, 1.0f)},
        Vertex{vec3(s, -s, d), vec2(1.0f, 0.0f)},
        Vertex{vec3(-s, s, d), vec2(0.0f, 1.0f)},
        Vertex{vec3(s, s, d), vec2(1.0f, 1.0f)},
        Vertex{vec3(s, -s, d), vec2(1.0f, 0.0f)}};
    generateArrayBuffer(quadVerticesBuffer, quadVertices);

    // state of the interface
    char screenshotName[128] = "file name";
    char materialName[128] = "material";
    bool fixScatterWidth = false;
    bool fixRgbDependentTransmission = true;
    bool fixRgbDependentBlurring = true;
    vector<int> materialNo(10, 0);

    static int currentFileName = 0;
    const char *fileNameList[] = {
        "bunny_downloaded_org",

        "cube_my_smooth",

        "cylinder",

        "monkey_my_cut",
        "monkey_my_smooth",
        "sphere_my",
        "squad_my"};

    const char *filterSizes[] = {"1", "3", "5", "7", "9", "11"};
    const char *lightTypes[] = {"directional light", "point light"};
    const char *textureList[] = {"none", "2D texture", "3D texture"};
    const char *animations[] = {"none", "rotate light source"};
    const char *specularModels[] = {"Phong", "Blinn-Phong", "Cook-Torrance"};
    const char *absorptionFunctions[] = {
        "exponential = exp(-5x)",
        "     linear = 1 - x",
        "  quadratic = 1 - x^2",
        "      cubic = 1 - x^3",
        "square root = sqrt(x)",
        " cubic root = cbrt(x)",
        "gaussian = exp(-5x^2"};

    /* Setup scene once, or for each test */
    setupScene(runTestCase);
    int iterations = runTestCase ? test.maxNumberOfIterations * test.maxNumberOfRuns : 1;

    for (int i = 0; i < iterations; i++)
    {
        if (runTestCase)
            test.setupObjects(i);

        if (!setupObjects(runTestCase, test))
            return EXIT_FAILURE;

        oldTime = glfwGetTime();

        /* start rendering loop: run while the close flag is not set */
        while (!glfwWindowShouldClose(window))
        {
            if (runTestCase)
                drawDepthPass = test.getDrawDepthPass();

            // start measuring time
            newTime = glfwGetTime();

            glfwPollEvents();

            /* update light object */
            if (!runTestCase)
                objects[0].setToPosition(sphericalToEuklidean(lights[0].sphericalPosition));

            /* start frame for imgui */
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            /* create window */
            {
                ImGui::Begin("Settings");
                {
                    /* Screenshot and select current object */
                    {
                        ImGui::InputText("", screenshotName, IM_ARRAYSIZE(screenshotName));

                        if (ImGui::Button("save screenshot"))
                        {
                            std::time_t result = std::time(nullptr);
                            string timestamp = string(asctime(localtime(&result)));
                            timestamp.pop_back();
                            saveScreenshotToFile("../pictures/screenshots/" + string(screenshotName) /*+ "_" + timestamp*/ + ".tga", winWidth, winHeight);
                        }

                        ImGui::SameLine();

                        ImGui::Checkbox("White Background color?", &backgroundIsWhite);

                        ImGui::Separator();
                        ImGui::SliderInt("current object", &currentObject, 0, objects.size() - 1);
                    }

                    if (ImGui::BeginTabBar("tab bar"))
                    {
                        if (ImGui::BeginTabItem("Scene"))
                        {
                            /* Light properties per light source */
                            for (int j = 0; j < lights.size(); j++)
                            {
                                ImGui::SliderFloat("Light Azimuth " + j, &lights[j].sphericalPosition[0], -M_PI, M_PI);
                                ImGui::SliderFloat("Light Zenith " + j, &lights[j].sphericalPosition[1], -M_PI / 2, M_PI / 2);
                                ImGui::SliderFloat("Light Radius " + j, &lights[j].sphericalPosition[2], 2, 10);

                                ImGui::ListBox("select light type" + j, &lights[j].type,
                                               lightTypes, IM_ARRAYSIZE(lightTypes));

                                ImGui::ColorEdit3("light0 color " + j, (float *)&lights[j].color);
                            }

                            ImGui::Separator();

                            ImGui::ListBox("Select animation", &animation,
                                           animations, IM_ARRAYSIZE(animations));

                            ImGui::Checkbox("[d] Draw to texture space", &drawToUVCoords);
                            ImGui::EndTabItem();
                        }

                        if (ImGui::BeginTabItem("Object"))
                        {
                            // texturing and object base color
                            ImGui::Checkbox("Texture mapping after TSD", &textureMappingAfterBlurring);
                            ImGui::ListBox("Choose texture type", &objects[currentObject].material.textureMappingNo,
                                           textureList, IM_ARRAYSIZE(textureList));

                            ImGui::ColorEdit3("base color of model", &objects[currentObject].material.color[0]);
                            ImGui::ListBox("switch 2D texture", &objects[currentObject].material.textureNo2d,
                                           textures2dList, IM_ARRAYSIZE(textures2dList));
                            ImGui::ListBox("switch 3D texture", &objects[currentObject].material.textureNo3d,
                                           textures3dList, IM_ARRAYSIZE(textures3dList));

                            // choose predefined material from csv
                            if (ImGui::ListBox("Choose Material", &materialNo[currentObject],
                                               materialNamesList, materials.size()))
                            {
                                objects[currentObject].material = materials[materialNo[currentObject]];
                                cout << "current object tx2d " << objects[currentObject].material.textureMappingNo << endl;
                                fixRgbDependentTransmission = false;
                                fixRgbDependentBlurring = false;
                            }

                            ImGui::InputText("Material name", materialName, IM_ARRAYSIZE(materialName));
                            if (ImGui::Button("Save material"))
                            {
                                objects[currentObject].material.name = materialName;
                                materials.push_back(objects[currentObject].material);
                                writeMaterials();

                                // update names list
                                for (int i = 0; i < materials.size(); i++)
                                {
                                    materialNamesList[i] = new char[materials[i].name.size() + 1];
                                    strcpy(materialNamesList[i], materials[i].name.c_str());
                                }
                            }

                            // choose object mesh
                            ImGui::ListBox("", &currentFileName, fileNameList, IM_ARRAYSIZE(fileNameList));
                            if (ImGui::Button("Load mesh"))
                            {
                                objects[currentObject].fileName = fileNameList[currentFileName];
                                if (!objects[currentObject].setupBuffers())
                                {
                                    cout << "error setting up buffers" << endl;
                                    return EXIT_FAILURE;
                                }
                            }

                            ImGui::Checkbox("Shadow mapping", &objects[currentObject].material.shadowMappingEnabled);
                            ImGui::SliderFloat("[d] scale along normal", &objects[currentObject].material.growVertexAlongNormalFactor, 0.0f, 0.1f);
                            ImGui::SliderFloat("[d] treshold shadow mapping", &objects[currentObject].material.shadowMappingTreshold, 0.0f, 0.05f);
                            ImGui::EndTabItem();
                        }

                        if (ImGui::BeginTabItem("Diffuse"))
                        {
                            ImGui::SliderFloat("diffuse constant (kd)", &objects[currentObject].material.kd, 0, 1);
                            ImGui::SliderFloat("roughness Oren-Nayar",
                                               &objects[currentObject].material.roughnessOn, 0, 1);
                            ImGui::SliderFloat("wrap constant (w)", &objects[currentObject].material.wrap, 0, 1);

                            ImGui::Separator();
                            ImGui::Separator();

                            ImGui::SliderFloat("scatter width (sw)", &objects[currentObject].material.scatterWidth, 0, 1);

                            ImGui::Text("set sw = w / (1 + w)");
                            ImGui::SameLine();
                            if (ImGui::Button("set") || fixScatterWidth)
                            {
                                objects[currentObject].material.scatterWidth = objects[currentObject].material.wrap / (1 + objects[currentObject].material.wrap);
                            }
                            ImGui::SameLine();
                            ImGui::Checkbox("fix", &fixScatterWidth);

                            ImGui::ColorEdit3("scatter color (sc)", &objects[currentObject].material.scatterColor[0]);
                            ImGui::EndTabItem();
                        }

                        if (ImGui::BeginTabItem("Specular"))
                        {
                            ImGui::ListBox("Select specular \nlighting model", &objects[currentObject].material.specularModel,
                                           specularModels, IM_ARRAYSIZE(specularModels));

                            ImGui::SliderFloat("specular/F0 before blurring (ks0)", &objects[currentObject].material.ks0, 0, 1);
                            ImGui::SliderFloat("specular/F0 after blurring (ks1)", &objects[currentObject].material.ks1, 0, 1);

                            if (objects[currentObject].material.specularModel == 2)
                            {
                                ImGui::SliderFloat("roughness (r)", &objects[currentObject].material.roughnessCt, 0, 1);
                                ImGui::SliderFloat("F(0) for Fresnel", &objects[currentObject].material.fresnelCt, 0.001, 1);
                            }
                            else
                                ImGui::SliderFloat("shininess (m)", &objects[currentObject].material.shininess, 1, 100);

                            ImGui::EndTabItem();
                        }

                        if (ImGui::BeginTabItem("Transmission"))
                        {
                            ImGui::SliderFloat("Transmission factor (kt)", &objects[currentObject].material.kt, 0.0f, 1.0f);
                            ImGui::ColorEdit3("Transmssion Color (ct)", &objects[currentObject].material.transmissionColor[0]);

                            ImGui::Checkbox("Bind RGB channels", &fixRgbDependentTransmission);

                            if (fixRgbDependentTransmission)
                            {
                                ImGui::SliderFloat("Absorption Factor (a)", &objects[currentObject].material.absorptionParameter[0], 0.01, 50);
                                objects[currentObject].material.absorptionParameter[1] = objects[currentObject].material.absorptionParameter[0];
                                objects[currentObject].material.absorptionParameter[2] = objects[currentObject].material.absorptionParameter[0];
                            }
                            else
                                ImGui::SliderFloat3("Absorption Factor (a)", &objects[currentObject].material.absorptionParameter[0], 0.01, 50);

                            ImGui::ListBox("Select absorption \nfunction t(d) with\nthickness d in [0,1], x = a*t",
                                           &objects[currentObject].material.absorptionFunction,
                                           absorptionFunctions, IM_ARRAYSIZE(absorptionFunctions));

                            ImGui::Separator();
                            ImGui::Separator();

                            ImGui::SliderFloat("TSM alpha1: use col_i?", &objects[currentObject].material.alpha1, 0, 1);
                            ImGui::SliderFloat("TSM alpha2: use wdot(n_i, l)?", &objects[currentObject].material.alpha2, 0, 1);
                            ImGui::SliderFloat("TSM alpha3: use transmission color", &objects[currentObject].material.alpha3, 0, 1);
                            ImGui::EndTabItem();
                        }

                        if (ImGui::BeginTabItem("Blurring"))
                        {
                            if (ImGui::ListBox("Filter size",
                                               &objects[currentObject].material.filterSize, filterSizes, IM_ARRAYSIZE(filterSizes)))
                            {
                                float tmpSigma = (2 * objects[currentObject].material.filterSize + 1) / 4.0f;
                                objects[currentObject].material.sigma = vec3(tmpSigma);
                            }

                            ImGui::Checkbox("Bind RGB channels", &fixRgbDependentBlurring);
                            if (fixRgbDependentBlurring)
                            {
                                ImGui::SliderFloat("Sigma for Gaussian", &objects[currentObject].material.sigma[0], 0.5, 5);
                                objects[currentObject].material.sigma[1] = objects[currentObject].material.sigma[0];
                                objects[currentObject].material.sigma[2] = objects[currentObject].material.sigma[0];
                            }
                            else
                                ImGui::SliderFloat3("Sigma for Gaussian", &objects[currentObject].material.sigma[0], 0.5, 5);

                            ImGui::Separator();

                            ImGui::EndTabItem();
                        }

                        ImGui::EndTabBar();
                    }

                    // ImGui frame rate measurement
                    float frameRate = ImGui::GetIO().Framerate;
                    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / frameRate, frameRate);

                    ImGui::End();
                }

                /* render */
                ImGui::Render();

                display(quadVerticesBuffer, drawDepthPass, runTestCase);
                onIdle(runTestCase, test);

                ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

                glfwSwapBuffers(window);
            }

            // end test run
            if (runTestCase && test.runtimeSeconds >= test.maxRuntime)
            {
                if (!test.nextTestIteration(window))
                {
                    break;
                }
            }

            oldTime = newTime;
        }
    }

    /* Cleanup imGui*/
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    /* Cleanup window */
    glfwDestroyWindow(window);
    glfwTerminate();

    test.writeResultsToFile();

    cout << "Bye!" << endl;
    return EXIT_SUCCESS;
}
