
uniform sampler2D irradianceMap;
uniform vec2 fUvCoords;

vec2 direction = horizontal ? vec2(1 / irradianceMapResolution, 0) : vec2(0, 1 / irradianceMapResolution);
float coefficientSum = 0;

vec4 myLookup(vec2 uvCoords, float coeff)
{
    vec4 lookup = texture(irradianceMap, uvCoords);
    float maskLookup = lookup.a;
    vec3 color = lookup.xyz; 

    color = clamp(color, 0, 1);
    maskLookup = clamp(maskLookup, 0, 1);

    if (maskLookup > 0)
    {
        coefficientSum += coeff;
        return vec4(color * coeff, maskLookup);
    }

    return vec4(0);
}

void main()
{
    float weight = 1;
    
    vec4 lookup = myLookup(fUvCoords, weight);
    vec4 avgValue = lookup;

    for (int i = 1; i <= m.filterSize; i++)
    {
        weight = exp(- 0.5 * i * i / (sigma * sigma));

        avgValue += myLookup(fUvCoords + i * direction, weight);
        avgValue += myLookup(fUvCoords - i * direction, weight);
    }

    vec3 diffuseColor = avgValue.xyz / coefficientSum;
    fragColor = vec4(diffuseColor, avgValue.a);
}
