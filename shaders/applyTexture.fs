layout (location = 0) uniform sampler2D normalRenderingMap;

layout (location = 11) uniform sampler2D texture2d;
layout (location = 1) uniform sampler2D depthMapColor;
layout (location = 2) uniform sampler2D depthMapNormal;
layout (location = 3) uniform sampler3D texture3d;

void main()
{
    /* looup texture */
    vec4 lookup = clamp(texture(normalRenderingMap, fUvCoords), 0, 1);

    /* vectors for lighting model */
    vec3 position = (mat.MVMatrix * vec4(fPosition, 1.0f)).xyz;
    vec3 v = normalize(-position);
    vec3 n = normalize((mat.NormalMatrix * vec4(normalize(fNormal), 1.0f)).xyz);

    /* thickness and shadow mapping */
    vec4 positionLightMVP = mat.LightMVPMatrix * vec4(fPosition, 1);
    vec2 projCoords = positionLightMVP.xy / positionLightMVP.w * 0.5 + 0.5;

    float d_o = length(mat.LightMVMatrix * vec4(fPosition, 1)) / 20;
    float d_i = (m.alpha1 <= 0) ? 
        texture(depthMapNormal, projCoords.xy).y 
        : texture(depthMapColor, projCoords.xy).a;
    
    float thickness = max(d_o - d_i, 0);
    bool inShadow = thickness >= m.shadowMappingTreshold;

    /* sum up I_p for all light sources */
    vec3 specularSum = vec3(0);
    vec3 I_p = lookup.xyz;

    for (int i = 0; i < numberLights; i++)
    {
        vec3 l = getLightVector(position, i);

        if (!inShadow || !m.shadowMappingEnabled || i != 0)
        {
            /* specular color */
            float fr_specular = specularReflectedLight(l, v, n);
            specularSum += m.ks1 * fr_specular * lights[i].color;
        }
    }

    fragColor = vec4(I_p + specularSum, 1);
}
