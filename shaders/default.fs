
layout (location = 0) uniform sampler2D texture2d;
layout (location = 1) uniform sampler2D depthMapColor;
layout (location = 2) uniform sampler2D depthMapNormal;
layout (location = 3) uniform sampler3D texture3d;

in float distVertex;
layout (location = 1) out vec4 fragNormal; // second output depth pass

/** The lighting model. Supports directional and point lights, Phong 
    and Blinn-Phong lighting, shadow mapping, texture mapping, wrap lighting.
    
    returns: vec4
        RGB channels contains color of diffuse light
        A channel contains the specular term */
void lightingModel(float d_o, vec4 lookupColor, vec4 lookupNormal)
{
    vec3 position = (mat.MVMatrix * vec4(fPosition, 1.0f)).xyz;

    vec3 v = normalize(-position);
    vec3 n = normalize((mat.NormalMatrix * vec4(normalize(fNormal), 1.0f)).xyz);
    
    /* texture mapping */
    vec3 objectColor = m.color;
    if (!enableDepthPass ||  m.alpha1 > 0)
    {
        switch(m.textureMappingNo)
        {
            case 1: objectColor = texture(texture2d, fUvCoords).xyz; break;
            case 2: objectColor = texture(texture3d, (fPosition + 1) / 2).xyz; break;
            default: break;
        }
    }

    /* write to depth map */
    if (enableDepthPass)
    {
        vec3 l = getLightVector(position, 0);
        float wCos = wrapFunction(dot(n, l), m.wrap);

        if (m.alpha1 <= 0 && m.alpha2 <= 0)
        {
            fragNormal.y = d_o;
        }

        else if (m.alpha1 <= 0 && m.alpha2 > 0)
        {
            fragNormal.xy = vec2(wCos, d_o);
        }
        
        else if (m.alpha1 > 0 && m.alpha2 <= 0)
        {
            fragColor = vec4(objectColor, d_o);  
        }

        else
        {
            fragColor = vec4(objectColor, d_o); 
            fragNormal.x = wCos;
        }
        
        return;
    }

    /* sum up I_p for all light sources */
    vec3 I_p = vec3(0);

    /* shadow mapping and thickness */
    float d_i = (m.alpha1 <= 0) ? lookupNormal.y : lookupColor.a;
    float thickness = max(d_o - d_i, 0);
    bool inShadow = thickness >= m.shadowMappingTreshold;

    for (int i = 0; i < numberLights; i++)
    {
        vec3 l = getLightVector(position, i);
        float wCos = wrapFunction(dot(n, l), m.wrap);

        /* init lighting model */
        vec3 I_diffuse = vec3(0), I_transmitted = vec3(0);
        float fr_specular = 0, ks0 = m.ks0, ks1 = m.ks1;

        if (!inShadow || !m.shadowMappingEnabled || i != 0)
        {
            /* specular amount */
            fr_specular = specularReflectedLight(l, v, n);

            /* scatter color */
            vec3 I_wrapped = scatter(wCos, m.scatterWidth) * m.scatterColor;

            /* diffuse color */
            float fr_diffuse = diffuseReflectedLight(l, v, n, wCos);
            I_diffuse = m.kd * (fr_diffuse * objectColor + I_wrapped);

            // Gamma correction?
        }

        if (i == 0)
        {
            I_transmitted = transmittedLight(objectColor, 
                lookupColor.xyz, lookupNormal.x, thickness);
        }

        I_p += (myMax(I_diffuse, I_transmitted) + ks0 * fr_specular) * lights[i].color;
    
        /* after specular term after blurring if enabled but no tsd */
        if (!enableTextureSpaceDiffusion)
        {
            I_p += vec3(ks1 * fr_specular) * lights[i].color;
        }
    }

    /* finally include light color, write mask in alpha channel */
    fragColor = vec4(I_p, 1);
}

void main()
{
    float d_o = length(mat.LightMVMatrix * vec4(fPosition, 1)) / 20;

    if (enableDepthPass)
    {
        lightingModel(d_o, vec4(0), vec4(0));
    }
    else
    {
        // perform perspective projection to lookup depth map
        vec4 positionLightMVP = mat.LightMVPMatrix * vec4(fPosition, 1);
        vec2 projCoords = positionLightMVP.xy / positionLightMVP.w * 0.5 + 0.5;
        
        // only look up what is really needed
        vec4 lookupColor = vec4(0), lookupNormal = vec4(0);

        if (m.alpha1 <= 0 && m.alpha2 <= 0)
        {
            lookupNormal.y = texture(depthMapNormal, projCoords.xy).y;
        }
        else if (m.alpha1 <= 0 && m.alpha2 > 0)
        {
            lookupNormal.xy = texture(depthMapNormal, projCoords.xy).xy;
        }
        else if (m.alpha1 > 0 && m.alpha2 <= 0)
        {
            lookupColor = texture(depthMapColor, projCoords.xy);
        }
        else
        {
            lookupColor = texture(depthMapColor, projCoords.xy);
            lookupNormal.x = texture(depthMapNormal, projCoords.xy).x;
        }

        lightingModel(d_o, lookupColor, lookupNormal);
    }
}
