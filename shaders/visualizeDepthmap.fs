
/* file: visualizeDepthmap.fs
belongs to visualizeDepthmap.vs

*/

layout (location = 0) uniform sampler2D depthMapColor;
layout (location = 1) uniform sampler2D depthMapNormal;

void main()
{
    vec3 color = texture(depthMapColor, fUvCoords).xyz;
    fragColor = vec4(vec3(color), 1.0);
}