void main()
{
    fUvCoords = vUvCoords;
    fNormal = vNormal;
    fPosition = vPosition;
    
    if (drawToUVCoords)
    {
        vec2 newUvCoords = vUvCoords * 2 - 1;
        gl_Position = vec4(newUvCoords, 0, 1.0);
    }
    else 
    {
        gl_Position = mat.MVPMatrix * vec4(vPosition, 1.0);
    }
}