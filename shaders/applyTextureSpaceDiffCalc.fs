
layout (location = 0) uniform sampler2D normalRenderingMap;

vec2 direction = horizontal ? vec2(1.0f / mapResolution, 0) : vec2(0, 1.0f/ mapResolution);
vec3 coefficientSum = vec3(0);

vec4 myLookup(vec2 uvCoords, vec3 coeff)
{
    vec4 lookup = texture(normalRenderingMap, uvCoords);
    float maskLookup = lookup.a;
    vec3 color = lookup.xyz; 

    color = clamp(color, 0, 1);
    maskLookup = clamp(maskLookup, 0, 1);

    if (maskLookup > 0)
    {
        coefficientSum += coeff;
        // color = color / maskLookup;
        return vec4(color * coeff, maskLookup);
    }

    return vec4(0);
}

void main()
{
    // incremental computation of the Guassian (GPUGems 3 Chapter 40)
    vec3 g0, g1, g2;
    vec3 weights = vec3(1);
    vec4 lookup, avgValue;

    const bool incrementalGaussian = true;

    if (incrementalGaussian)
    {
        for (int j = 0; j < 3; j++)
        {
            g0[j] = 1 / (m.sigma[j] * sqrt(2 * PI));
            g1[j] = exp(-0.5 / (m.sigma[j] * m.sigma[j]));
            g2[j] = g1[j] * g1[j];
        }

        weights = g0;
    }
    
    lookup = myLookup(fUvCoords, weights);
    avgValue = lookup;

    for (int i = 1; i <= m.filterSize; i++)
    {
        if (incrementalGaussian)
        {
            g0 *= g1;
            g1 *= g2;

            weights = g0;
        }
        else
        {
            for (int j = 0; j < 3; j++)
            {
                weights[j] = exp(- 0.5 * i * i / (m.sigma[j] * m.sigma[j]));
            }
        }

        avgValue += myLookup(fUvCoords + i * direction, weights);
        avgValue += myLookup(fUvCoords - i * direction, weights);
    }

    vec3 diffuseColor = avgValue.xyz / coefficientSum;
    fragColor = vec4(diffuseColor, avgValue.a /* lookup.a */);
}
