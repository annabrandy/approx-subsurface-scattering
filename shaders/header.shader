
#version 450

#define MAX_FILTER_SIZE 9
#define MAX_LIGHT_NUMBER 3
#define PI 3.141592653589793

layout (location = 12) uniform bool enablePhongInDepth;

layout (location = 13) uniform bool enableTextureSpaceDiffusion;
layout (location = 14) uniform bool enableDepthPass;
layout (location = 15) uniform int mapResolution;

layout (location = 16) uniform int numberLights = 1;

layout (location = 18) uniform bool horizontal;
layout (location = 19) uniform bool drawToTexture;

layout (location = 22) uniform bool textureMappingAfterBlurring = false;
layout (location = 23) uniform bool drawToUVCoords;

layout (location = 4) uniform struct Matrices
{
    mat4 ViewMatrix;
    mat4 MVMatrix;
    mat4 MVPMatrix;
    mat4 NormalMatrix;

    // for the first light source, which calulates transmission
    mat4 LightMVMatrix;
    mat4 LightMVPMatrix;
} mat;

layout (location = 30) uniform struct Light
{
    vec3 position;
    vec3 color;
    int type;
} lights[MAX_LIGHT_NUMBER];

layout (location = 40) uniform struct Material
{
    // color of material
    vec3 color;

    // lighting model parameters
    float kd; // albedo
    float ks0;
    float ks1;
    float shininess;
    float roughnessOn;
    float roughnessCt;
    float fresnelCt;

    // Phong, Blinn-Phong, Cook-Torrance
    int specularModel;

    // wrap lighting
    float wrap;
    float scatterWidth;
    vec3 scatterColor;

    // transmission
    float kt;
    int absorptionFunction;
    vec3 absorptionParameter;
    vec3 transmissionColor;
    float alpha1;
    float alpha2;
    float alpha3;

    // filter
    int filterSize;
    vec3 sigma;

    // others
    bool shadowMappingEnabled;
    int textureMappingNo;

    // debugging values for shadow mapping
    float growVertexAlongNormalFactor;
    float shadowMappingTreshold;

} m;
