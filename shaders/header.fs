
in vec3 fPosition;
in vec3 fNormal;
in vec2 fUvCoords;

layout (location = 0) out vec4 fragColor;

/** Returns component-wise maximum of two vectors */
vec3 myMax(vec3 a, vec3 b)
{
    a = clamp(a, 0, 1);
    b = clamp(b, 0, 1);
    return vec3(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
}

/** Returns component-wise minumum of two vectors */
vec3 myMin(vec3 a, vec3 b)
{
    a = clamp(a, 0, 1);
    b = clamp(b, 0, 1);
    return vec3(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
}

/** Returns the normalized light vector l pointing from the given position to 
    the light source. Calculation depends on light source.
    
    Supported light types:
    - 0: point light
    - 1: directional light
*/
vec3 getLightVector(vec3 position, int lightNo)
{
    switch(lights[lightNo].type)
    {
        case 0: return normalize(lights[lightNo].position);
        case 1: vec4 lightPos = mat.ViewMatrix * vec4(lights[lightNo].position, 1.0);
            return normalize(lightPos.xyz - position);
    }
}

/** Returns the scatter value, which is the part of the object which should 
    contain a color shift. There are two versions. Scatter is my adaptation,
    scatterOrg is like the one from the book. */
float scatter(float x, float sw)
{
    if (sw <= 0.0) return 0.0;
    return smoothstep(0.0, sw, x) * (1 - smoothstep(sw, 2.0 * sw, x));
}

float scatterOrg(float x, float sw)
{
    if (sw <= 0.0) return 0.0;
    return smoothstep(0.0, sw, x) * smoothstep(2.0 * sw, sw, x);
}

/** Returns the wrapped cosine of n and l. */
float wrapFunction(float x, float w)
{
    return clamp((x + w) / (1 + w), 0, 1);
}

/* Cook-Terrance Lighting Model*/

// ggx
float normalDistributionFunction(float cosTheta, float alpha)
{
    float num = alpha * alpha;
    float denom = cosTheta * cosTheta * (alpha * alpha - 1) + 1;
    return num / (PI * denom * denom);
}

// Schlick-Beckmann
float geometryFunctionHelper(float cosTheta, float k)
{
    float num = cosTheta;
    float denom = cosTheta * (1 - k) + k;
    return num / denom;
}

// Smith equations
float geometryFunction(float NdotV, float NdotL, float alpha)
{
    // float k = pow(alpha + 1, 2) / 8.0;
    float k = alpha * sqrt(2 / PI);

    // NdotV and NdotL or LdotH and VdotH
    float g1 = geometryFunctionHelper(NdotV, k);
    float g2 = geometryFunctionHelper(NdotL, k);
    return g1 * g2;
}

/* For non-metallic surfaces */
float fresnelSchlickEquation(float cosTheta, float F0)
{
    return F0 + (1.0f - F0) * pow(1.0f - cosTheta, 5.0f);
}

/** Evaluates the specular reflection function light, view and 
    normal vector. Additionally, the lighting constants are given too, 
    they are manipulated by 

    Supported specular reflection functions:
    - 0: Phong
    - 1: Blinn-Phong
    - 2: Cook-Torrance
*/
float specularReflectedLight(vec3 l, vec3 v, vec3 n)
{
    if (m.ks0 <= 0 && m.ks1 <= 0)
        return 0;
    
    float specular = 0; 

    if (m.specularModel == 0)
    {
        vec3 r = 2 * n * dot(n, l) - l;
        float specularDot = clamp(dot(r, v), 0, 1);
        specular = pow(specularDot, m.shininess);
    }
    else if (m.specularModel == 1)
    {
        // change to version from principles
        vec3 h = normalize(l + v);
        float specularDot = clamp(dot(n, h), 0, 1);
        specular = (8 + m.shininess) / (8 * PI) * pow(specularDot, 4 * m.shininess);
    }
    else if (m.specularModel == 2)
    {
        float alpha = max(0.001, m.roughnessCt * m.roughnessCt);
        
        vec3 h = normalize(l + v);
        float NdotV = clamp(dot(n, v), 0, 1);
        float NdotH = clamp(dot(n, h), 0, 1);
        float NdotL = clamp(dot(n, l), 0, 1);
        float HdotV = clamp(dot(h, v), 0, 1);

        float D = normalDistributionFunction(NdotH, alpha);
        float G = geometryFunction(NdotL, NdotV, alpha);
        float F = fresnelSchlickEquation(HdotV, m.fresnelCt);

        float num = F * D * G;
        float denom = 4 * NdotV * NdotL;
        specular = num / max(denom, 0.001);
    }

    return specular;
}

/** Evaluates the diffuse reflection function for a given light, view and 
    normal vector. The wCos/cos value is given too because it was already
    calculated.

    Supported diffuse reflection functions:
    - 0: Lambertian
    - 1: Oren-Nayar
*/
float diffuseReflectedLight(vec3 l, vec3 v, vec3 n, float wCos)
{
    if (m.kd <= 0)
    {
        return 0;
    }
    else if (m.roughnessOn <= 0)
    {
        return wCos;
    }
    else
    {
        float sigma = m.roughnessOn;
        float sigma2 = sigma * sigma;

        // A and B: only once per sigma change
        float A = 1 - 0.5 * sigma2 / (sigma2 + 0.33);
        float B = 0.45 * sigma2 / (sigma2 + 0.09);

        // float thetaNL = acos(dot(n, l));
        float thetaNL = acos(wCos);
        float thetaNV = acos(dot(n, v));

        float alpha = max(thetaNL, thetaNV);
        float beta = min(thetaNL, thetaNV);

        float sinAlpha = sin(alpha);
        float tanBeta = tan(beta); // replace tan

        // replace cos(a - b) = cos(a) * cos(b) + sin(a) *sin(b)
        float phiL = atan(l.x / l.z);
        float phiV = atan(v.x / v.z);

        float cosPhi = cos(phiL - phiV);
    
        return wCos * (A + B * max(cosPhi, 0) * sinAlpha * tanBeta);
    }

    return 0;
}

/** Maps the given thickness to a certain transmission value according to 
    the strictly decreasing absorption function.
    
    Supported absorption functions:
    - 0: exonential
    - 1: linear
    - 2: quadratic
    - 3: cubic
    - 4: sqare root
    - 5: cube root
*/
vec3 getTransmittedFactor(float thickness)
{
    vec3 x = m.absorptionParameter* thickness;
    vec3 t = vec3(0);

    switch (m.absorptionFunction)
    {
        case 0: t = exp (- 5 * x); break;
        case 1: t = 1 - x; break; 
        case 2: t = 1 - pow(x, vec3(2)); break; 
        case 3: t = 1 - pow(x, vec3(3)); break;
        case 4: t = 1 - sqrt(x); break;
        case 5: t = 1 - pow(x, vec3(1/3.0)); break;
        case 6: t = exp(- 5 * pow(x, vec3(2)));
    }
    return clamp(t, 0, 1);
}

/** Evaluates the transmitted term. It contains a linear interpolation 
    between information on this and on the other side. */
vec3 transmittedLight(vec3 c_o, vec3 c_i, float wCos_i, float thickness)
{
    if (m.kt <= 0)
        return vec3(0);
    
    vec3 td3 = getTransmittedFactor(thickness);
    float td1 = (td3.x + td3.y + td3.z) / 3.0f;

    vec3 c =  mix(c_o, c_i, m.alpha1);
    float wCos =  mix(1, wCos_i, m.alpha2);
    vec3 transmissionColor = mix(c, (1 - td1) * m.transmissionColor, m.alpha3);

    return max(m.kt * wCos * td3 * transmissionColor, vec3(0));
}
