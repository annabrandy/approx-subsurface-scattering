
/* file: visualizeDepthmap.vs
belongs to visualizeDepthmap.fs

*/

void main()
{
    fUvCoords = vUvCoords;
    gl_Position = vec4(vPosition, 1.0);
}