
out float distVertex;

void main()
{
    fPosition = vPosition;
    fNormal = vNormal;
    fUvCoords = vUvCoords;

    distVertex = length(mat.LightMVMatrix * vec4(vPosition, 1));

    if(enableDepthPass)
    {
        /* scale position along vertex to avoid self shadowing effects */
        float grow = m.growVertexAlongNormalFactor;
        vec3 vPositionScaled = vPosition + vNormal * grow;

        /* calculate position from light point of view */
        gl_Position = mat.LightMVPMatrix * vec4(vPositionScaled, 1);
    }

    else if(enableTextureSpaceDiffusion || drawToUVCoords)
    {
        vec2 newUvCoords = vUvCoords * 2 - 1;
        gl_Position = vec4(newUvCoords, 0, 1.0);
    }

    else
    {
        gl_Position = mat.MVPMatrix * vec4(vPosition, 1.0);
    }

}
